import request from '@/utils/request';

/**
 * 查询客户端管理列表
 * @param query
 * @returns {*}
 */

export function listClient(query) {
  return request({
    url: '/system/client/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询客户端管理详细
 * @param id
 */
export function getClient(id) {
  return request({
    url: '/system/client/' + id,
    method: 'get'
  });
};

/**
 * 新增客户端管理
 * @param data
 */
export function addClient(data) {
  return request({
    url: '/system/client',
    method: 'post',
    data: data
  });
};

/**
 * 修改客户端管理
 * @param data
 */
export function updateClient(data) {
  return request({
    url: '/system/client',
    method: 'put',
    data: data
  });
};

/**
 * 删除客户端管理
 * @param id
 */
export function delClient(id) {
  return request({
    url: '/system/client/' + id,
    method: 'delete'
  });
};

/**
 * 状态修改
 * @param id ID
 * @param version 乐观锁
 * @param status 状态
 */
export function changeStatus(id, version, status) {
  const data = {
    id, version, status
  };
  return request({
    url: '/system/client/changeStatus',
    method: 'put',
    data: data
  });
}
