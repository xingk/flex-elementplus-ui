import request from '@/utils/request'

// 查询产品树列表
export function listProduct(query) {
  return request({
    url: '/mf/product/list',
    method: 'get',
    params: query
  })
}

// 查询产品树详细
export function getProduct(productId) {
  return request({
    url: '/mf/product/' + productId,
    method: 'get'
  })
}

// 新增产品树
export function addProduct(data) {
  return request({
    url: '/mf/product',
    method: 'post',
    data: data
  })
}

// 修改产品树
export function updateProduct(data) {
  return request({
    url: '/mf/product',
    method: 'put',
    data: data
  })
}

// 删除产品树
export function delProduct(productId) {
  return request({
    url: '/mf/product/' + productId,
    method: 'delete'
  })
}
